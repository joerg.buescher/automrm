read_sampleinfo <- function(smpl, prm) {

  # reads out sample.info for types to use in anovo
  # returns some funny 'type'-variables
  #

  tryCatch({
    cat('\n Reading additional sample info ', file=prm$log_con)
  } , error = function(err) {
    print('\n Reading additional sample info ')
  })
  

  if (file.exists(file.path(prm$batchdir, 'sample.info'))){
    sampleinfo <- read.csv(file.path(prm$batchdir, 'sample.info'), stringsAsFactors = FALSE)
    activetypes <- character()
    testtypes <- c('type1','type2','type3','common_name','celltypename','cultivationname')
    for (itn in 1:length(testtypes)){
      if (length(unique(sampleinfo[, testtypes[itn]] )) > 1) {
        activetypes <- c(activetypes, testtypes[itn])
      }
    }

    nowtype <- character()
    sorttype <- character()
    additionalinfo <- matrix(' ', nrow=(1+nrow(sampleinfo)), ncol = 8)
    additionalinfo[1,] <- c('group1', 'group2', 'group3', 'amount','cultivation','organism','celltype','date')
    if (!is.null(sampleinfo[1,'plateposition'])) {
      additionalinfo <- matrix(' ', nrow=(1+nrow(sampleinfo)), ncol = 9)
      additionalinfo[1,] <- c('group1', 'group2', 'group3', 'amount','cultivation','organism','celltype','date','platepos')
    }
    for (id in 1:prm$nsmpl) {
      #samplenum <- as.numeric(gsub("([0-9]+).*$", "\\1", smpl$samplenames[id])) # Grep leading IDs from samplenames
      samplenum <- as.numeric(gsub("[[:alpha:]]", '', substring(smpl$samplenames[id],1,8))) # quick bugfix for pipeline
      samplepos <- which(sampleinfo[ ,1] == samplenum)
      smpl$type1[id] <- sampleinfo[samplepos,'type1']
      smpl$type2[id] <- sampleinfo[samplepos,'type2']
      smpl$type3[id] <- sampleinfo[samplepos,'type3']
      smpl$amount[id] <- sampleinfo[samplepos,'amount']
      smpl$cultivationname[id] <- sampleinfo[samplepos,'cultivationname']
      smpl$common_name[id] <- sampleinfo[samplepos,'common_name']
      smpl$celltypename[id] <- sampleinfo[samplepos,'celltypename']
      smpl$measureddate[id] <- sampleinfo[samplepos,'measureddate']
      smpl$plateposition[id] <- sampleinfo[samplepos,'plateposition']
      smpl$nowtype[id] <- paste0(sampleinfo[samplepos,'type1'], sampleinfo[samplepos,'type2'], sampleinfo[samplepos,'type3'], sampleinfo[samplepos,'common_name'], sampleinfo[samplepos,'celltypename'], sampleinfo[samplepos,'cultivationname'])

      smpl$sorttype[id] <- c(toString(sampleinfo[samplepos,activetypes]) , '-')[1]
    }
  }

  smpl

} # endfunction
