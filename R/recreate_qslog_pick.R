#' Create better training data for peak picking
#' 
#' With the additional knowledge of the manually selected peaks, we can now create better training data for the peak picking model
#' 
#' @param metab automRm list holding metabolite information
#' @param smpl automRm list holding sample information
#' @param msd automRm list holding intermediate data for processing
#' @param prm automRm list holding parameters required for processing
#' @param xlsx_path string pointing towards training_solution.xlsx file
#' 
#' @export

recreate_qslog_pick <- function(msd, metab, smpl, prm, xlsx_path) {
  logstart = TRUE
  # read training data xlsx with col names in cells
  td <- openxlsx::read.xlsx(xlsx_path, colNames = FALSE, rowNames = FALSE)
  filecol <- which(tolower(td[2,]) == 'data file')
  for (im in 1:prm$nmet){
    met_index <- which(tolower(td[1,]) == tolower(metab[[im]]$name ))
    if (length(met_index) == 1) {
      rtcol <- which(tolower(td[2, ]) == 'rt')
      rtcol <- min(rtcol[rtcol >= met_index -1], na.rm = TRUE)
      startcol <- which(tolower(td[2, ]) == 'int. start')
      startcol <- min(startcol[startcol >= met_index-1], na.rm = TRUE)
      endcol <- which(tolower(td[2, ]) == 'int. end')
      endcol <- min(endcol[endcol >= met_index-1], na.rm = TRUE)
      nowusetrace <- c(max(0,metab[[im]]$quant$use, na.rm = TRUE), 
                       max(0,metab[[im]]$qual12c$use, na.rm = TRUE), 
                       max(0,metab[[im]]$qual13c$use, na.rm = TRUE) )
      for (id in 1:prm$nsmpl){
        spl_index <- which(tolower(td[ ,filecol]) == paste0(tolower(smpl$samplenames[id]), '.d') )
        if (length(spl_index) == 1) {
          # get manually integrated peak
          mpeaktop <- which.min(abs(prm$unirt - as.numeric(td[spl_index,rtcol])))
          if (length(mpeaktop) > 0) {
            qs_scores <- get_quality_scores(x1=msd[[im]][[id]]$x1, x2=msd[[im]][[id]]$x2, x3=msd[[im]][[id]]$x3, 
                                            peakstart=which.min(abs(prm$unirt - as.numeric(td[spl_index,startcol]))), 
                                            peaktop = mpeaktop, 
                                            peakend=which.min(abs(prm$unirt - as.numeric(td[spl_index,endcol]))),
                                            nowusetrace, im, metab, prm)
            if (logstart){
              qslog <- c(metab[[im]]$name, smpl$samplenames[id], 1, td[spl_index,rtcol],
                         as.numeric(qs_scores), msd[[im]][[id]]$foundchrom[2:3])
              logstart = FALSE
            } else {
              qslog <- rbind(qslog, c(metab[[im]]$name, smpl$samplenames[id], 1, td[spl_index,rtcol],
                                      as.numeric(qs_scores), msd[[im]][[id]]$foundchrom[2:3]))
            }
            
            # get additional peaks outside the manually integrated range as negative examples
            x1slope <- diff(msd[[im]][[id]]$x1)
            x1switch <- diff(sign(x1slope))
            local_max <- which(x1switch == -2)
            local_min <- which(x1switch == 2)
            # top5 local maxima
            peaktops <- local_max[order(msd[[im]][[id]]$x1[local_max], decreasing=T)][1:prm$initial_candidates]
            # other than manually selected one
            peaktops <- peaktops[c(which(peaktops < (mpeaktop - 3) ) ,
                                   which(peaktops > (mpeaktop + 3) ) )  ] 
            
            if (length(peaktops) > 0){
              peakstarts <- peakends <- numeric()
              for (i in 1:length(peaktops)){
                # nearest local minimum left from peaktop
                peak_min_start <- max(c(local_min[sign(local_min - peaktops[i]) < 0],1))
                # maximum range of bad slope cutoff left from peaktop
                peak_start_width_thresh <- round(peaktops[i] - (peaktops[i] - peak_min_start)/3)
                # bad slope indices between potential peakstart and peaktop
                bad_slopes <- which(abs(x1slope[peak_min_start:peak_start_width_thresh]) < 
                                      max(abs(x1slope[peak_min_start:peak_start_width_thresh]),na.rm=T)*0.1)
                # x-shift of peakstart (0 if bad_slopes is empty)
                shift <- max(c(0,bad_slopes))
                peakstarts[i] <- max(c(1,peak_min_start + shift))
                
                # nearest local minimum right from peaktop
                peak_max_end <- min(c(local_min[sign(local_min - peaktops[i]) > 0],length(msd[[im]][[id]]$x1)))
                # maximum range of bad slope cutoff right from peaktop
                peak_end_width_thresh <- round(peak_max_end - (peak_max_end - peaktops[i])/3)
                # bad slope indices between potential peakstart and peaktop
                bad_slopes <- which(abs(x1slope[peak_max_end:peak_end_width_thresh]) < 
                                      max(abs(x1slope[peak_max_end:peak_end_width_thresh]),na.rm=T)*0.1)
                # x-shift of peakend (0 if bad_slopes is empty)
                shift <- max(c(0,bad_slopes))
                peakends[i] <- min(c(peak_max_end - shift),length(msd[[im]][[id]]$x1))
              }
             
              # add 3 additional non-peaks as negative examples
              # tryCatch({
              if (length(peaktops) > 1){
                for (i in 1:3) {
                  nowtop <- sample(peaktops,1)
                  nowstart <- nowend <- c(peakends, peakends, peakstarts, peakstarts, peaktops)
                  nowstart <- nowstart[nowstart <= nowtop]
                  nowstart <- sample(nowstart,1)
                  nowend <- nowend[nowend >= nowtop]
                  nowend <- sample(nowend,1)
                  peaktops <- c(peaktops, nowtop)
                  peakstarts <- c(peakstarts, nowstart)
                  peakends <- c(peakends, nowend)
                }
              }
              # }, error = {
              #   print(paste('peaktops <- c(', paste(peaktops, collapse = ','), ')'))
              #   print(paste('peakends <- c(',paste(peakends, collapse = ','), ')'))
              #   print(paste('peakstarts <- c(',paste(peakstarts, collapse = ','), ')'))
              # })
              
              # get quality scores and add to list
              for (i in 1:length(peaktops)){
                qs_scores <- get_quality_scores(x1=msd[[im]][[id]]$x1, x2=msd[[im]][[id]]$x2, x3=msd[[im]][[id]]$x3, 
                                                peakstart = peakstarts[i], 
                                                peaktop = peaktops[i], 
                                                peakend = peakends[i],
                                                nowusetrace, im, metab, prm)
                qslog <- rbind(qslog, c(metab[[im]]$name, smpl$samplenames[id], i + 1, prm$unirt[peaktops[i]],
                                        as.numeric(qs_scores), msd[[im]][[id]]$foundchrom[2:3]))
              }
            } # endif there is a mpeaktop
          } # endif are there any peaks
        } # endif is there a matching sample
        
      } # endfor all samples
    } # endif is there a matching metabolite
  } # endfor all mets
  if (!exists('qs_scores')){
    print('Warning: re-creation of qslog_pick.tsv failed')
  } else {
    colnames(qslog) <- c("Metabolite", "id", "Peak", "RT", names(qs_scores) , "12C", "13C")
    write.table(qslog, file = file.path(substr(xlsx_path,1,nchar(xlsx_path)-23), 'qslog_pick.tsv'), sep = '\t', row.names = FALSE)
  }
}