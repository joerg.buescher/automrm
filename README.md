# Before you start...

## Recommendations for data acquisition
For optimal performance of automRm, data should be recorded in MRM mode or scheduled MRM mode. Ideally, samples are spiked with 13C labled internal standards of each metabolite. An good way to achieve this is to spike in fully 13C labeled extracts of similar samples (if such samples can be generated). An affordable and generally applicable alternative is the addition of commercially available fully 13C labeled yeast metabolite extract (https://isotopic-solutions.com/).

For each metabolite, a quantifier and at least one qualifier should be recorded. automRm can evaluate one unlabeled qualifier and one 13C labeled qualifier. All qualifiers are expected to perfectly co-elute with the quantifier.

## Example data
You can find examples of all files mentioned here in our demodata repository
https://gitlab.gwdg.de/joerg.buescher/demodata

## Video tutorial
You can find video tutorials describing the use of automRm on vimeo:
* Using automRm from the command line: https://vimeo.com/681364369 
* Using automRm with the GUI: https://vimeo.com/681366086 

## Citation
When you use automRm, please cite our publication (yes, it's open access!):

Eilertz, D.; Mitterer, M.; Buescher, J. M. 
AutomRm: An R Package for Fully Automatic LC-QQQ-MS Data Preprocessing Powered by Machine Learning.
Anal. Chem. 2022, 94 (16), 6163–6171. 
https://doi.org/10.1021/acs.analchem.1c05224.

# Functions

**Looking for a description of the GUI? Please scroll to the bottom of this page.**

## process_batch
Fully automatic detection and integration of peaks of \*.mzML files.

### Requirements:
- Raw LC-QQQ-MS data files in \*.mzML format, in one folder.
- update\_prm.tsv (see below for details) in the same folder as the \*.mzML files.
- An \*.xlsx file with metabolite definitions (see below).



### The key steps performed by process\_batch are:
- Definition of default parameters and updating them based on information update\_prm.tsv and/or using the function argument `parameters`.
- Parsing of metabolite definitions from the given .xlsx file.
- Parsing of \*.mzML files.
- Initial peak detection independently for each sample and each metabolite. For each peak candidate quality scores are calculated (see below). Subsequently, the peak picking ML model is used to predict the best peak candidate. If no ML model is present (when `ml_type` is set to &quot;mltrain\_pick&quot;), the peak with the highest sum of all calculated quality scores is choosen as best peak. The sample for which the sum of the predicted peak candidate scores is highest is subsequently used as reference sample for peak alignment.
- Peak alignment across samples: The shift in retention time relative to the reference sample is determinded for all metabolites and all samples. Subsequently, the aligned chromatograms of one metabolite from all samples are summed and peak detection is performed once for each metabolite on this &quot;consensus&quot; chromatogram. Candidate peaks are scored just like for initial peak detection. Finally, the peak borders of the best consensus peak candidate are propagated back to the original chromatogram for every sample.
- Normalization of peak heights and areas to 13C qualifier.
- Each peak's quality scores plus additional batch-wide quality scores are fed to the peak reporting ML model to predict if a peak&#39;s quality is good enough to be reported to non-expert scientists. Hereby, the overall peak quality is not only quantified for each measurement but also considering all other measurements of a specific metabolite in a given batch. Only peaks with a classification probabilility of at least 0.5 are finally reported.
- Plotting of all chromatograms to peakoverview.pdf.
- Writing of analysis summary (e.g. peak heights and areas) to peakinfo.xlsx.
- Writing of automRmdata\_Review.RData for optional manual peak review using automRm::automrm\_gui().

### Quality scores used for both ML models:
- Averaged correlation of quantifier and up to 2 qualifiers (QS\_cor123).
- Ratio of peak top to highest point outside the peak in quantifier chromatogram (QS\_T.o).
- Ratio of peak top to highest point outside the peak in unlabeled qualifier chromatogram (QS\_T.o2).
- Ratio of peak top to highest point outside the peak in 13C labeled qualifier chromatogram (QS\_T.o3).
- Ratio of peak top to the higher peak border (QS\_T.h).
- Ratio of peak top to the lower peak border (QS\_T.l).
- Deviation of expected retention time as defined in metabolite definitions (QS\_dRT).
- Deviation of expected retention after peak alignment (QS\_dRTs).
- Peak width (QS\_w).
- Correlation of quantifier to unlabeled qualifier (QS\_cor12).
- Correlation of quantifier to 13C labeled qualifier (QS\_cor13).
- Correlation of unlabeled qualifier to 13C labeled qualifier (QS\_cor23).
- Peak height (QS\_height).
- Ratio of retention time to total length of chromatographic gradient (QS\_relRT).
- Correlation of peak to gauss peak (QS\_gauss).
- Ratio of measured and pre-set ratios of signal intensity of quantifier and unlabeled qualifier (QS\_1vs2).

Quality scores used only for peak reporting ML model:

- 0th, 25th, 50th, 75th and 100th percentiles of peak picking ML model's classification scores of all samples for one metabolite.
- Score of peak picking ML model of one sample and one metabolite.

### Arguments:
- `nowfolder`: The working directory that contains all relevant input files. Automatically generated result files will be stored here (optional - default: The current working directory from which `process_batch` is called).
- `parameters`: Global default parameters defined in `initialize_prm` or updated via update\_prm.tsv can finally be overwritten by the `parameters` argument. The argument must be a list of value pairs, representing the variable name to be overwritten and its corresponding value (optional – default: list() ).
- `return_data`: Optional Boolean flag to return the resulting data structure which holds all relevant information about the peaks of each metabolite and each sample (optional - default: FALSE).

<br>

## train\_model
`train_model` trains the peak picking and/or reporting ML model for automated metabolomics pre-processing.

The function `train_model` is used to train and save ML models for both, the peak picking ML model as well as the peak reporting ML model. Both ML models should be trained initially before using `process_batch`.

Firstly, the peak picking ML model has to be trained. This model is used to predict the likelihood of given peak candidates to represent the target metabolite. The peak candidate with the highest classification probability is then used during downstream analysis for each metabolite and each sample. The selected peaks are subsequently stored in qslog\_pick.tsv and manual\_peakcheck\_template.xlsx and visualized in peakoverview.pdf in order to train the peak reporting ML model.

After the peak picking model has been trained and stored as \*.RData file, the peak reporting ML model can be trained. Preliminary, manual\_peakcheck\_template.xlsx has to be filled out in order to serve as training solution. Herefore, detected peaks displayed in peakoverview.pdf can be evaluated to update respective values in the corresponding manual\_peakcheck\_template.xlsx (0: do not report peak, 1: ignore peak during training, 2: report peak). After all values have been inserted, the file must be renamed from manual\_peakcheck\_template.xlsx to manual\_peakcheck.xlsx. It is recommended to set the global variable `expertmode` to TRUE using update\_prm.tsv when training both ML models. In this case, resulting peak graphs show a detailed summary of quality scores for each sample and each metabolite.

<br>

### Requirements:
- Raw LC-QQQ-MS data files in *.mzML format, in each `datasets` subfolder.
- update\_prm.tsv (see below for details) in the same `datasets` subfolder as the *.mzML files.
- An .xlsx file with metabolite definitions for each dataset that should be used for training.
- qslog\_pick.tsv in each of the listed ``datasets`` subdirectories in case of training the peak picking ML model (optional – see below).
- tsv in each of the listed `datasets` subdirectories in case of training the peak reporting ML model (optional – see below).
- training_solution.xlsx in each of the listed `datasets` in case of training the peak picking ML model (see below).
- manual\_peakcheck.xlsx in each of the listed `datasets` subdirectories in case of training the peak reporting ML model (see below).

<br>

### The key steps performed by train\_model() are:
- Checking if tab separated files with training data are present in each of the given `datasets` subfolders. If is no such file present, `process_batch` is called to generate respective training data before model training is continued. In case of the peak picking ML model `train_model` is looking for the file qslog\_pick.tsv, in case of the peak reporting ML model train\_model() is looking for the file qslog\_report.tsv.
- Checking if training solutions are present in each of the &#39;datasets&#39; subfolders. `train_model` can only be run if each of the given dataset subfolders contains files with training solutions (training\_solution.xlsx for the peak picking ML model and manual\_peakcheck.xlsx for the peak reporting ML model).
- Once training data and training solution data are present in all `datasets` folders, respective files are read:
  - For the peak picking ML model, retention times of all peak candidates for each sample and each metabolite listed in qslog\_pick.tsv are checked against their corresponding peakstarts and peakends stored in training_solution.xlsx. If the peak candidate&#39;s retention time lies in between the start and end of the reference peak, this candidate is considered a hit. If multiple candidates fall within the boundaries of the respective reference peak, the candidate with the smallest difference between corresponding retention times is chosen as a hit. All other peak candidates are subsequently tagged as fail.
  - For the peak reporting ML model, the peaks and their quality scores listed in qslog\_report.tsv are checked against the manually inserted flags of each manual\_peakcheck.tsv (0: do not report peak, 1: ignore peak for training 2: report peak). Subsequently, quartiles as well as the minimum and maximum of quality scores for each batch and each metabolite are computed and appended to the training data. This means that each peak gets 5 additional values for the ML model to be trained against: The 0th, 25th, 50th, 75th and 100th percentile of the corresponding batch&#39;s quality scores.
- Afterwards, resulting training data is split up into training (80 percent) and testing data (20 percent) for model evaluation after training.
- Missing quality scores for individual peaks or peak candidates are imputed by median values of the respective quality scores in the training data set.
- Quality scores and respective percentiles (when training the peak reporting ML model) are scaled, centered and transformed using the Yeo-Johnson transformation.
- The chosen ML model (either Random Forest, Neural Network, Support Vector Machines or Extreme Gradient Boosting) is trained using 3-fold cross validation and ROC-curve as optimization metric.
- The resulting ML model is saved as \*.RData file in `base_folder`. In case of the peak picking ML model, median values of each quality score from the training data set are appended to the \*.RData file. These median values are needed for imputation during subsequent runs of `process_batch` if calculated quality scores have invalid values (e.g. 0 or NA).
- For evaluation, the ML model is in turn used to predict the testing data set. Optionally, model evaluation metrics are visualized/summarized in various graphs which are automatically stored as .pdf or .txt files in the `base_folder`:
  - Feature density: The plot shows the distribution of each feature (quality scores and their batch-wide percentiles [in case of the peak reporting model]) for each classification group (blue: no peak, pink: peak). The graph can be utilized as a first intuition about the impact of each quality score on the models ability to correctly separate cases of hit and fail. The feature density plot is created firstly for the training data and secondly for the testing data, the latter is based on evaluated model predictions.
  - False negatives/false positives: The *.txt files show all peaks or peak candidates which have been falsely classified as hit (false positive) or fail (false negative) and their respective features (quality scores and their batch-wise percentiles in case of the peak reporting ML model).
  - Confusion matrix: The confusion matrix shows the summary of correctly and falsely classified peaks as well as several evaluation metrics (e.g. model accuracy and sensitivity/specificity).
  - Prediction density plot: Probability density distributions for all model predictions.
  - Feature importance: The influence of each quality score on model predictions.
- In case of training the peak picking ML model, `process_batch` is run directly after the model has been trained. `process_batch` will then use the saved ML model for peak picking predictions to generate the files peakoverview.pdf and manual\_peakcheck\_template.xlsx. These files can subsequently be used to manually prepare the training data for the peak reporting ML model.

<br>

### Arguments:
- `model_type`: &quot;pick&quot; (peak picking ML model) or &quot;report&quot; (peak reporting ML model)
- `ml_type`: Either &quot;rf&quot; (Random Forest [default]) , &quot;nnet&quot; (Neural Network), &quot;svm&quot; (Support Vector Machines) or &quot;xgbtree&quot; (Extreme Gradient Boosting)
- `base_folder`: Working directory that contains named subfolders with training dataset(s).
- datasets: Subfolders of `base_folder` that must contain all relevant files to run `process_batch` which is called by `train_model`.
- `model_file_name`: Tag to automatically name resulting files like \*.RData-files, .txt-files and graphs.
- `print_stats`: Boolean indicating if ML model evaluation statistics should be written (default = FALSE).

<br>


## initialize\_prm
Loading and assignment of global default parameters. Default parameters can be overwritten by the `parameters` argument when calling `process_batch` or using edited update\_prm.tsv files in the working directory).

| **Parameter name** | **Default Value** | **Alternative Values** | **Comment** |
| --- | --- | --- | --- |
| `ml_type` | mlprod | mltrain\_pick, mltrain\_report | Use `process_batch` in productive mode (default) or in the context of ml\_train |
| `expertmode` | FALSE | TRUE | Additional debugging output |
| `fancyformat` | FALSE | TRUE | Add conditional formatting to .xlsx output. Caution, might be incompatible with some versions of MS Excel. |
| `verbose` | 1 | 0, 2, 3 | Levels of debugging output |
| `log_con` | file(&quot;R\_messages.log&quot;, open=&quot;a&quot;) | |Connection to log file |
| `samplingfrequency` | 2 | Positive integer | Frequency in Hz for re-sampling of chromatograms. Should be similar to the rate at which the data has been recorded |
| `corwindow` | 5 | Positive integer | Number of data points used for calculating correlation among chromatograms |
| `smoothnum` | 9 | Positive integer | K for rollmean smoothing of chromatograms |
| `minDistTopBorder` | 2 | Positive integer | Minimum number of scans between peak top and peak border |
| `bgoffset` | 42 | Positive integer | Smallest possible signal intensity |
| `minNormArea` | 10000 | Positive integer | Minimum area of 13C qualifier to perform normalization |
| `minwidth` | 4 | Positive integer | Minimum width of peak in seconds |
| `initial_candidates` | 5 | Positive integer | Number of peak candidates to be evaluated during initial peak picking |
| `timewindow` | 1.5 | Positive float | Time window in minutes around peak used for retention time alignment |
| `tshiftcrude` | seq(-60,60,2) | Integer range | Range of allowed retention time shifts |
| `tshiftfine` | seq(-30,30,1) | Integer range | Range of allowed retention time shifts |
| `microsearch_in` | 6 | Integer | Number of scans for fine-adjustment of peak borders during propagation of consensus peak to original chromatograms going from propagated peak boarder towards peak top |
| `microsearch_out` | 12 | Integer | Number of scans for fine-adjustment of peak borders during propagation of consensus peak to original chromatograms going from propagated peak boarder outwards away from peak top |
| `pick_model_path` ||| Filename (incl. path) of file holding peak picking ML model |
| `report_model_path` | || Filename (incl. path) of file holding peak reporting ML model |

<br>

# Input data

## Nowfolder (choosen working directory)
- **mzML files:**
  - Measurement files must be present in the working directory in mzML format and are read using the mzR package.
<br>

- **sample.info:**
  - sample.info is an optional file for the attachment of additional information to the samples that are processed, such as biological and technical conditions. This information is then used for grouping during the analysis of variance and the generation of the peak report (Peakinfo.xlsx). sample.info is loaded from the working directory and must be present in the form of a comma separated file (other names as sample.info are not allowed). The file must consist of 11 columns and the number of rows must be identical to the number of samples that are going to be analyzed.
  - sample.info must contain the following comma-separated columns:
    - _idmeasurements_: the unique identifier of the \*.mzMl filename (required).
    - _samplename_: the sample name (required).
    - amount: Amount of the material in the sample tube (optional).
    - type1: Used for sample annotation during data analysis. Samples with identical values will be treated as replicates (optional).
    - type2: Used for sample annotation during data analysis. Samples with identical values will be treated as replicates (optional).
    - type3: Used for sample annotation during data analysis. Samples with identical values will be treated as replicates (optional).
    - common\_name: Name of the organism from which the sample was generated (optional).
    - celltypename: Name of celltype of which the sample was generated (optional).
    - cultivationname: Name of the cultivation condition that was used to generate the sample (optional)
    - measureddate: Date and time of the measurement. Must be present in the format YYYY-MM-DD HH:MM:SS (optional)
    - plateposition: Position of the sample tube on the plate (optional)
<br>

- **update\_prm.tsv:**
  - update\_prm.tsv can be used to update global settings of the analysis. The file is loaded from the working directory and must be present in the form of a tab separated file (other names as update\_prm.tsv are not allowed).
  - There must be exactly two tab separated column names:
    - `Variable in prm`
    - `Value`
  - All entries must contain (I) the name of the `prm` element to be replaced and (II) the respective value to overwrite the default settings (see list of available `prm` options).
  - Special case input of ranges: For `tshiftcrude` and `tshiftfine` an integer can be entered that is then interpreted as range. For example `6` is interpreted as `seq(-6,6,1)`.
  - Alternatively, `prm` options can be overwritten with the argument `parameters` when calling the `process_batch` function. When `process_batch` is called with given `parameters`, the default settings and values set in update\_prm.tsv are overwritten.
<br>

- **training_solution.xlsx:**
  - The file training_solution.xlsx is used to train the peak picking ML model, which is applied for initial peak detection. The file is required when `train_model` is called with the function argument `model_type` set to &quot;pick&quot;.
  - This file can be easily generated by expert peak review in Agilent MassHunter Quantitative Analysis or similar software or using `automRm::automrm_gui()`.
  - training_solution.xlsx must be present in the working directory (other names than training_solution.xlsx are not allowed) and is loaded during model training.
  - The first two rows of the file must contain the necessary column names. Columns are hierarchically structured. The first row must contain the following columns:
    - Sample section: Samples are defined in the Data File column.
    - List of metabolite names: The given column names must exactly match those given in the metabolite database set in update\_prm.tsv.
  - The second row must contain the following columns:
    - Data File: Names of the measurement files listed in the working directory (File extensions are automatically omitted).
    - RT: Peak Retention time of the specific metabolite in minutes.
    - Start: Peak start time in minutes.
    - End: Peak end time in minutes.
<br>

- **qslog\_pick.tsv:**
  - qslog\_pick.tsv is used to train the peak picking ML model which is used for initial peak detection.
  - The file is automatically created in case of no present ML models. When the `prm` setting `ml_type` is set to &quot;mltrain\_pick&quot; 5 peak candidates are detected for each metabolite and each sample.
  - Quality scores of each peak candidate are calculated and written to qslog\_pick.tsv which is automatically saved in the working directory.
  - The file&#39;s columns contain information about the metabolite, the sample, the peak candidate number, its retention time and all calculated quality scores.
  - qslog\_pick.tsv is read during `train_model` when the function argument `model_type` is set to &quot;&#39;pick&quot;&#39;.
<br>

- **manual\_peakcheck.xlsx:**
  - The file manual\_peakcheck.xlsx is used to train the peak reporting ML model, which is used for final peak evaluation. The file is required when `train_model` is called with the function argument `model_type` set to &quot;report&quot;.
  - The file should be created by the user before the peak reporting ML model is trained. When `process_batch` is run using the peak picking ML model, a template file (manual\_peakcheck\_template.xlsx) is automatically created and saved in the working directory. The file already contains all necessary information except the peaks final classification score. The user can now use the template to fill out the score columns for each metabolite. Hereby the file peakoverview.pdf can be used as a visual reference.
  - manual\_peakcheck.xlsx must be present in the working directory (other names than manual\_peakcheck.xlsx are not allowed) and is loaded during model training.
  - The first two rows of the file must contain the necessary column names. Columns are hierarchically structured. The first row must contain the following columns:
    - Names of the measurement files listed in the working directory.
    - List of the metabolite names: The given column names must exactly match those given in the metabolite database set in update\_prm.tsv.
  - The second row must contain the following columns:
    - Score: Should the chosen peak be reported (0: no, 1: ignore peak, 2: yes)
    - Start: Peak start time in minutes.
    - End: Peak end time in minutes.
<br>

- **qslog\_report.tsv**
  - qslog\_report.tsv is used to train the peak reporting ML model which is applied to decide if a given peak should finally be reported.
  - The file is automatically created when the peak picking ML model is already trained and used for peak detection by running `process_batch`. When the `prm` setting `ml_type` is set to &quot;mltrain\_report&quot; the best peak of each metabolite and each sample is chosen by the peak picking ML model and based on the peak&#39;s quality scores.
  - The chosen peaks are then written to qslog\_report.tsv which is saved in the current working directory. The file&#39;s columns contain information about the metabolite, the sample and the chosen peak&#39;s quality scores.
  - qslog\_report.tsv is read during `train_model` when the function argument `model_type` is set to &quot;&#39;report&quot;&#39;.

## Other

- **Metabolite database (.xlsx file):**
  - The metabolite database is used to specify information about the metabolites to be detected by `process_batch`.
  - The table should be present in xlsx format and is automatically read in during `process_batch`. The path of the file must be set using either the parameter argument in the function call of `process_batch` or the assignment of the .xlsx file variable in update\_prm.tsv.
  - The file must contain the following columns:
    - Name: Name of the metabolite (string format).
    - HMDB: ID of the metabolite in the Human Metabolite Database (string format).
    - KEGG: ID of the metabolite in the Kyoto Encyclopedia of Genes and Genomes (string format).
    - RT: Expected retention time of the metabolite (double format in minutes)
    - Quantifier: Flag if metabolite should be handled as quantifier (integer format, 1: yes | 0: no).
    - UseTrace: Flag if a metabolite&#39;s quantifier/qualifier should be used for the calculation of peak quality scores (integer format, 1: yes | 0: no).
    - Isoptope: Flag if a metabolite&#39;s quantifier/qualifier should be handled as 12C or 13C isotope (string format, &#39;12C&#39; | &#39;13C&#39;).
    - Polarity: Polarity of measurement. The polarity flag is crucial to read out the respective MRM of the \*.mzML file (string format, &#39;Positive&#39; | &#39;Negative&#39;).
    - Q1mz: Expected mass to charge ratio for the first quadrupole. This column is crucial to read out the respective MRM of the \*.mzML file (double format).
    - Q3mz: Expected mass to charge ratio for the second quadrupole. This column is crucial to read out the respective MRM of the \*.mzML file (double format)
    - Abundance: Expected relative ratio of the metabolite&#39;s quantifier/qualifier compared to other quantifiers/qualifiers of the respective metabolite. (double format).
<br>

- **Peak picking ML model (`pick_model_path`):**
  - The peak picking ML model must be trained by `train_model` initially and is then stored as \*.RData file in the folder given by the function&#39;s argument `base_folder` and the filename set by the function&#39;s argument `model_file_name`.
  - Once trained, the peak picking ML model can be used to detect the best peak out of the 5 given candidates for each sample and each metbolite during `process_batch`. The peak picking ML model is not only used to predict peak qualities but also to prepocess raw quality scores in the forefield (missing data imputation and normalization). Therefore, `process_batch` has to be called with the argument `ml_type` set to &quot;mltrain\_report&quot;.
  - The path of the peak picking ML model file must be set using either the `parameter` argument in the function call of `process_batch` or the assigment of the `excelfile` variable in update\_prm.tsv.
<br>

- **Peak reporting ML model (`report_model_path`):**
  - The peak reporting model must be trained by `report_model_path` initially and is then stored as \*.RData file in the folder given by the function&#39;s argument `base_folder` and the filename set by the function&#39;s argument `model_file_name`.
  - Once trained, the peak reporting ML model can be used to evaluate if the peak which was detected by the peak picking ML model should be reported for each sample and each metabolite during `process_batch`. The peak reporting ML model is not only used to predict peak qualities but also to pre-process raw quality scores in the forefield (missing data imputation and normalization). Therefore, `process_batch` has to be called with the argument `ml_type` set to &quot;mlprod&quot;.
  - The path of the peak reporting ML model file must be set using either the `parameter` argument in the function call of `process_batch` or the assignment of the `excelfile` variable in update\_prm.tsv
<br>


# Output data

## Nowfolder (chosen working directory)
- **peakinfo.xlsx:**
  - peakinfo.xlsx holds the results of the analysis. The file's columns consist of (I) metainformation about the batch when provided by sample.info and (II) the metabolites listed in the metabolite database. All processed samples are listed row-wise showing the calculated abundances of each metabolite.
  - The spreadsheet is divided into eight tables:
    - NormArea.
    - NormHeight.
    - RawArea.
    - RawHeight.
    - 13Carea.
    - Info.
  - If `expertmode` == `TRUE` the file additionally contains
    - UnfilteredArea (all quantifier peak areas, not filtered by score of peak reporting ML model).
    - UnfilteredHeight (all quantifier peak heights, not filtered by score of peak reporting ML model).
    - Unfiltered13C (all 13C qualifier peak areas, not filtered by score of peak reporting ML model).
Unfiltered data is a best guess but should be used with great caution and is not suitable to be given to inexperienced users.
<br>

- **R.time:**
  - R.time is automatically written to the working directory and stores the analysis time of `process_batch`.
<br>

- **peakoverview.pdf:**
  - All chromatograms of measured metabolites (rows) in each sample (columns) are graphically shown in peakoverview.pdf.
  - Quantifier chromatograms are coloured in black, whereas 12C qualifiers are shown in blue and 13C qualifiers are coloured in green. The left x-axis refers to quantifier and 12C qualifier intensities (black and blue), the right one refers to qualifier intensities (green).
  - The legend in each plot shows the expected polarities and mass to charge ratios of each quantifier and qualifier (if present).
  - When running in expertmode, the labels of each graph show the names of the metabolite and sample as well as the detailed quality scores of each detected peak. In case expertmode is turned off, only the aggregated quality score of each peak is shown in the labels.
  - The expected retention time of each peak is visualized as red mark on the x-axis, the shifted retention time is shown as blue mark.
  - The calculated peak retention time for each metabolite and each sample is indicated as a vertical dotted line. The area of the peak is shaded gray.
<br>

- **R\_messages.log:**
  - R\_messages.R serves as the main file for logging the analysis of `process_batch` and all its subfunctions, depending on the chosen level of the global verbose parameter.
<br>

- **shiftplots.pdf:**
  - The PDF-file shows the shift of all metabolite peak&#39;s retention times for each sample.
  - The x-axis shows the peaks&#39; calculated retention times, the y-axis the respective shifts.
  - The calculated model used for peak aligment is visualized as line.
<br>

- **manual\_peakcheck\_template.xlsx:**
  - When `process_batch` is run using the peak picking ML model, a template file (manual\_peakcheck\_template.xlsx) is automatically created and saved in the working directory. The file already contains all necessary information except the peaks score. The user can now use the template to fill out the score columns for each metabolite. Hereby the file peakoverview.pdf can be used as a visual reference. After the training data has been inserted the file can then be saved as manual\_peakcheck.xlsx and will be processed to train the peak reporting ML model.
<br>

- **qslog\_pick.tsv:**
  - qslog\_pick.tsv is used to train the peak picking ML model which is used for initial peak detection.
  - The file automatically created in case of no present ML models. When the `prm` setting `ml_type` is set to `mltrain\_pick` n (default: n = 5) peak candidates are detected for each metabolite and each sample.
  - Quality scores of each peak candidate are calculated and and written to qslog\_pick.tsv which is automatically saved in the working directory.
  - The file&#39;s columns contain information about the metabolite, the sample, the peak candidate number, its retention time and all calculated quality scores.
  - qslog\_pick.tsv is read during `train_model` when the function argument `model_type` is set to &quot;&#39;pick&quot;&#39; .
<br>

- **qslog\_report.tsv:**
  - qslog\_report.tsv is used to train the peak reporting ML model which is used to decide if a given peak should finally be reported.
  - The file is automatically created when the peak candidate model is already trained and used for peak detection by running `process_batch`. When the `prm` setting `ml_type` is set to &quot;mltrain\_pick&quot; the best peak of each metabolite and each sample is chosen by the peak picking ML model and based on the peak&#39;s quality scores.
  - The chosen peaks are then written to qslog\_report.tsv which is saved in the current working directory. The file&#39;s columns contain information about the metabolite, the sample and the chosenchoosen peak&#39;s quality scores.
  - tsv is read during `train_model` when the function argument `model_type` is set to &quot;&#39;report&quot;&#39;.
<br>

## Other

- **Peak picking ML model (`pick_model_path`):**
  - The peak candidate model must be trained by `train_model` initially and is then stored as \*.RData file in the folder given by the function&#39;s argument `base_folder` and the filename set by the function&#39;s argument `model_file_name`.
  - Once trained, the peak picking ML model can be used to detect the best peak out of the n (default: n = 5) given candidates for each sample and each metabolite during `process_batch`. The peak picking ML model is not only used to predict peak qualities but also to preprocess raw quality scores beforehand (missing data imputation and normalization). Therefore, `process_batch` has to be called with the argument `ml_type` set to &quot;mltrain\_report&quot;.
  - The path of the peak picking ML model file must be set using either the `parameter` argument in the function call of `process_batch` or the assignment of the `excelfile` variable in update\_prm.tsv.
<br>

- **Peak reporting ML model (`report_model_path`):**
  - The peak reporting model must be trained by `train_model` initially and is then stored as \*.RData file in the folder given by the function&#39;s argument `base_folder` and the filename set by the function&#39;s argument `model_file_name`.
  - Once trained, the peak reporting ML model can be used to evaluate if the peak which was detected by the peak picking ML model should be reported for each sample and each metabolite during `process_batch`. The peak reporting ML model is not only used to predict peak qualities but also to prepocess raw quality scores (missing data imputation and normalization). Therefore, `process_batch` has to be called with the argument `ml_type` set to &quot;mlprod&quot;.
  - The path of the peak reporting ML model file must be set using either the `parameter` argument in the function call of `process_batch` or the assigment of the `excelfile` variable in update\_prm.tsv.
<br>



# automrm_gui
An interactive shiny app can be used to prepare training data for both, the peak picking and the peak reporting model. Additionally, final peak reporting predictions made by the peak reporting model in the course of `process_batch` can be manually modified to provide full control of process_batch's final output (peakoverview.pdf & peakinfo.xlsx). 

The function `automrm_gui` can be used to start the shiny app:

```{r}
automRm::automrm_gui()
```

![](inst/extdata/automrm_gui.png)


The app is organized in tabs: 
- Process
  - Process Batch: process batches of .mzML files and prepare data for training
  - Train Model: start training of ML models using previously prepared data sets
- Review
  - Manual Review: review and modify pre-processed data (hardly required if ML models are good enough)
  - Peak Picking Training: generate training solution for training of peak picking ML model
  - Peak Reporting Training: generate training solution for training of peak reporting ML model

## Process 
### Process Batch
The process batch tab is a handy interface to generate the "update_prm.tsv" file and start process_batch(). There are 4 mandatory inputs:
- Select metabdb: select .xlsx file with metabolite information.
- Select peak picking model: select .RData file with peak picking model.
- Select peak reporting model: select .RData file with peak reporting model
- Select a folder: Select folder that contains the batch of samples tha you would like to process in .mzML format

Clicking the blue "Process Batch" button triggers the generation of "update_prm.tsv" and starts process_batch.
Optionally you can set additional values that are passed on to process_batch() at the bottom of the page.

### Train Model
The train model tab asks for the required information to start training of a new ML model and then calls train_model().
- First, select a project folder that contains sub-folders that in turn contain your training data sets. You should have created training data for these data sets before using them to train a new ML model. Please refer to the section above on how to generate training data.
- Second, select the sub-folders that you want to use for training the new ML model.
- Third, select the type of ML model and your preferred algorithm. Optionally, you can give a custom name to the model file.
- Clicking the blue "Start Training" button starts the actual trainig in the background. The new ML models will be saved to the project folder when the training is complete.


## Review

The review section consists of **two main panels, a sidebar panel and a tab panel**.

### Sidebar panel
In the upper part of the sidebar panel you will see the controls that allow you to navigate your data set. In the lower part of the side bar panel you can upload the required files. This order might not seem intuitive at first, but it save mouse mileage :-)

- **automRmdata_initial.RData**: Initial peak candidates for each metabolite over all samples. Peaks have only been selected based on aggregated quality scores and without any model predictions. Serves as input to generate the output of the peak selection training tab (training_solution.xlsx)
 - **automRmdata_mltrain_prepare.RData**: Consenus peak candidates for each metabolite over all samples predicted by the peak picking model. Serves as input to generate the output of the peak reporting training tab (manual_peakcheck.xlsx) 
 - **automRmdata_Review.RData**: Final predictions based on the peak reporting model. This *.RData file can be loaded to provide the user full flexibility over the peaks to be reported in automRm's result files (peakinfo.xlsx and peakoverview.pdf).
 
Please start by loading the  **\*.RData** files containing your batch. To save disk space, the ML models are not included in these files but have to be loaded separately. For manual peak review, please also load a peak reporting model. For peak reporting training please load a peak picking model. For peak picking training the batch file is sufficient.

Once the peak data is loaded into the shiny app, the user can **navigate through samples** and metabolites using respective buttons and dropdown menus, as well as **arrow keys on the keyboard** and by clicking the overview plot in the bottom of the sidebar panel. 

Training and manipulation of loaded peak data can always be **saved as \*.RData file**, to continue working with this data set at some later point in time

The **overview plot** in the middle of the sidebar panel shows the matrix of all metabolites (columns) and samples (rows). You can click on a square in the overview plot to directly jump to the respective chromatogram.

### Plot panel
In the plot panel, each chromatogram can be directly accessed and displayed showing quantifiers and 12C/13C qualifiers. The shaded grey area indicates the currently selected peak region. Light grey shading indicates peaks that will not be reported while dark grey shading indicates peaks that will be reported. The vertically dotted line indicates the expected retention time as defined in the metabolite information .xlsx file that had been used for the processing of the samples. The current peak's overall quality score (mean quality score, peak picking classification probability or peak reporting classificatipn probability) is displayed next to the apex. 

#### Manual review tab
The **manual review tab** can be used to manually validate predictions made by the peak reporting model. Hereby, you have full control about which peaks of which sample and metabolite should be listed in automRm's output files such as peakinfo.xlsx and peakoverview.pdf. The resprective buttons can be used to trigger the generation of these files directly using the app and after all neccessary modifications have been performed by the user. 

- Integrate: integrate the peak as indicated in the plot and report the intensity value
- Zero: report intensity = 0
- Missing: do not report an intensity value
- Click-and-drag a range on the plot to define the peak range. Only the x-value (RT value) of the selected range will be evaluate.

Optionally, you can check the "Apply to al samples" box to apply the change to the current metabolite in all samples. Please use this function with care, it will overwrite any manual manipulation that you have previously done for this metabolite.


#### Peak selection training tab
The **peak selection training tab** offers the possibility to manually generate the training data for the peak picking model (training_solution.xlsx). To this end, initial peak detection data (automRmdata_initial.RData) has to be uploaded to the app. This file is generated when running `process_batch` initially without the usage of any priorly trained models (Please note that `ml_type` has to set to `mltrain_pick` and `verbose` has to be set to at least 2). Initially selected peaks solely rely on aggregated quality scores for each peak candidate. Peak borders can manually be edited with the mouse, also in case the wrong peak has been selected by `process_batch` in the first place. Peakstart, peakend and peaktop values are displayed below the chromatogram plot. Manual edits can be cancelled using the "Undo manual" button. The "No Peak" button can be used to indicate no present peak in the whole chromatogram. On the overview plot, rectangles show unviewed (white), viewed (grey) and manually edited chromatograms and respective peaks. Once all peaks are checked and edited, training data can be exported using the respective button.


#### Peak reporting training tab
The **peak reporting training tab** offers the possibility to manually generate the training data for the peak reporting model (manual_peakcheck.xlsx). Herefore, results by the peak picking model (automRmdata_mltrain_prepare.RData) have to loaded as input. This file is generated when running process_batch with the usage of predictions made by the peak picking model (Please note that `ml_type` has to set to either `mltrain_prepare` or `mltrain_report` and `verbose` has to be set to at least 2) .
On the overview plot, rectangles show peaks to ignore during training (grey), good peaks to report (green) and bad peaks that should not be reported (red). Default rectangle colors in the overview plot correspond to each classification prediction of the peak picking model: 

| Threshold  | classification | color |
| ---------  | -------------- | ----- |
| **> 0.95** | good peak | green |
| **< 0.2 & < 0.95** | ignore peak | gray |
| **< 0.2** | bad peak | red |

Buttons can be used to edit the peak reporting status that will be exported as training data using the respective buttons. Peaks to ignore are not used for model training. Alternatively, one can use the the following keys while navigating through the chromatograms: 
<kbd>g</kbd> (good peak) <kbd>b</kbd> (bad peak) <kbd>i</kbd> (peak to ignore)

Once all peaks are checked and edited, training data (manual_peakcheck.xlsx) can be exported using the respective button.
