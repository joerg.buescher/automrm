# Recommendations for data acquisition

For optimal performance of automRm, data should be recorded in MRM mode or scheduled MRM mode. Ideally, samples are spiked with 13C labled internal standards of each metabolite. The best way to achieve this is to spike in fully 13C labeled extracts of similar samples. Addition of commercially available fully 13C labeled yeast metabolite extract is also a good option (https://isotopic-solutions.com/).

For each metabolite, a quantifier and at least one qualifier must be recorded. automRm can evaluate one unlabeled qualifier and one 13C labeled qualifier. All qualifiers are expected to perfectly co-elute with the quantifier.

# Functions

## process\_batch

Fully automatic detection and integration of peaks of \*.mzML files..

<br>

### Requirements:

- Raw LC-QQQ-MS data files in \*.mzML format, in one folder.
- update\_prm.tsv (\*link) (see below for details) in the same folder as the \*.mzML files.
- An \*.xlsx file (\*link) with metabolite definitions (see below).

<br>

### The key steps performed by process\_batch are:

- Define default parameters and update them based on information update\_prm.tsv and/or using the function argument &#39;parameters&#39; (\*link).
- Parsing of metabolite definitions from the given Excel file.
- Parsing of \*.mzML files.
- Initial peak detection independently for each sample and each metabolite. For each peak candidate quality scores are calculated (see below). Subsequently the peak picking ML model is used to predict the best peak candidate. If no ML model is present (when &#39;ml\_type&#39; is set to &quot;mltrain\_pick&quot;), the peak with the highest sum of all calculated quality scores is choosen as best peak. The sample for which the sum of the predicted peak candidate scores is highest is subsequently used as reference sample for peak alignment.
- Peak alignment across samples (\*link): The shift in retention time relative to the reference sample is determineddeterminded for all metabolites and all samples. Subsequently, the aligned chromatograms of one metabolite from all samples are summed and peak detection is performed once for each metabolite on this &quot;consensus&quot; chromatogram. Candidate peaks are scored just like for initial peak detection. Subsequently, the peak borders of the best consensus peak candidate are propagated back to the original chromatograms for every sample.
- Normalization of peak heights and areas to 13C qualifier.
- The single-peak quality scores plus additional batch-wide quality scores are fed to the peak reporting ML model to predict if a peak&#39;s quality is good enough to report it to non-expert scientists. Hereby, the overall peak quality is not only quantified for each measurement but also considering all other measurements of a specific metabolite in a given batch. Only peaks with a classification probabillility ~~SCORE~~ of at least 0.5 are finally reported.
- Plotting of all chromatograms to peakoverview.pdf (\*link).
- Writing of excel filepeakinfo.xlsx (\*link) reporting peak height and peak area in raw and 13C-normalized.

<br>

### Quality scores used for both ML models:

- Averaged correlation of quantifier and up to 2 qualifiers (QS\_cor123).
- Ratio of peak top to highest point outside the peak in quantifier chromatogram (QS\_T.o).
- Ratio of peak top to highest point outside the peak in unlabeled qualifier chromatogram (QS\_T.o2).
- Ratio of peak top to highest point outside the peak in 13C labeled qualifier chromatogram (QS\_T.o3).
- Ratio of peak top to the higher peak border (QS\_T.h).
- Ratio of peak top to the lower peak border (QS\_T.l).
- Deviation of expected retention time as defined in metabolite definitions (QS\_dRT).
- Deviation of expected retention after peak alignment (QS\_dRTs).
- Peak width (QS\_w).
- Correlation of quantifier to unlabeled qualifier (QS\_cor12).
- Correlation of quantifier to 13C labeled qualifier (QS\_cor13).
- Correlation of unlabeled qualifier to 13C labeled qualifier (QS\_cor23).
- Peak height (QS\_height).
- Ratio of retention time to total length of chromatographic gradient (QS\_relRT).
- Correlation of peak to gauss peak (QS\_gauss).
- Ratio of measured and pre-set ratios of signal intensity of quantifier and unlabeled qualifier (QS\_1vs2).

Quality scores used only for peak reporting ML model:

- Quantiles of scores of peak picking ML model of all samples for one metabolite.
- Score of peak picking ML model of one sample and one metabolite.

<br>

### Arguments:

- nowfolder: The working directory that contains all relevant input files. Automatically generated result files will be stored here (optional - default: The current working directory from which process\_batch() is called).
- parameters: Global default parameaters defined in initialize\_prm() (\*link) or updated via update\_prm.tsv (\*link) can finally be overwritten by the &#39;parameters&#39; argument. The argument must be a list of value pairs, representing the variable name to be overwritten and its corresponding value (optional – default: list()).
- return\_msd: Optional Bboolean flag to return the resulting data structure which holds all relevant information about the peaks of each metabolite and each sample (optional - default: FALSE).

<br>

<br>

## train\_model

train\_model Train first and/or peak reporting ML model for automated peak picking.

The function train\_model() is used to train and save ML models for both, the peak picking ML model as well as the peak reporting ML model. Both ML models should be trained initially before using process\_batch().

Firstly, the peak picking ML model has to be trained. This model is used to predict the likelihood of given peak candidates to represent a valid peak. The peak candidate with the highest classification probability is then used during downstream analysis for each metabolite and each sample. The selected peaks are subsequently stored in qslog\_pick.tsv (\*link) and manual\_peakcheck\_template.xlsx (\*link) and visualized in peakoverview.pdf (\*link) in order to train the peak reporting ML model for peak reporting.

After the peak picking model has been trained and stored as \*.RData file, the peak reporting ML model can be trained. Preliminary, manual\_peakcheck\_template.xlsx (\*link) has to be filled out in order to serve as training solution. Herefore, detected peaks displayed in peakoverview.pdf can be evaluated to update respective values in the corresponing manual\_peakcheck\_template.xlsx (0: do not report peak, 1: no clear decision can be made, 2: report peak). After all values have been inserted, the file must be renamed from manual\_peakcheck\_template.xlsx to manual\_peakcheck.xlsx. It is recommended to set the global variable &#39;expertmode&#39; to &quot;TRUE&quot; using update\_prm.tsv when training both ML models. In this case, resulting peak graphs show a detailed summary of quality scores for each sample and each metabolite.

<br>

### Requirements:

- Raw LC-QQQ-MS data files in .mzML format, in each &#39;datasets&#39; subfolder.
- update\_prm.tsv (see below for details) in the same &#39;datasets&#39; subfolder as the .mzML files.
- An Excel file with metabolite definitions (\*link) for each dataset that should be used for training.
- qslog\_pick.tsv (\*link) in each of the listed &#39;datasets&#39; subdirectories in case of training the peak picking ML model (optional – see below).
- tsv (\*link) in each of the listed &#39;datasets&#39; subdirectories in case of training the peak reporting ML model (optional – see below).
- xlsx (\*link) in each of the listed &#39;datasets&#39; in case of training the peak picking ML model (see below).
- manual\_peakcheck.xlsx (\*link) in each of the listed &#39;datasets&#39; subdirectories in case of training the peak reporting ML model (see below).

<br>

### The key steps performed by train\_model() are:

- Checking if tab separated files with training data are present in each of the given &#39;datasets&#39; subfolders. If there is no such file present, process\_batch() is called to generate respective training data before model training is continued. In case of the peak picking ML model train\_model() is looking for the file qslog\_pick.tsv, in case of the peak reporting ML model train\_model() is looking for the file qslog\_report.tsv.
- Checking if training solutions are present in each of the &#39;datasets&#39; subfolders. train\_model() can only be run if each of the given dataset subfolders contains files with training solutions (training_solution.xlsx for the peak picking ML model and manual\_peakcheck.xlsx for the peak reporting ML model).
- Once training data and training solution data are present in all &#39;datasets&#39; folders, respective files are read:
  - For the peak picking ML model, retention times of all peak candidates for each sample and each metabolite listed in qslog\_pick.tsv are checked against the corresponding peakstarts and peakends stored in training_solution.xlsx. If the peak candidate&#39;s retention time lies in between the start and end of the reference peak, this candidate is considered a hit. If multiple candidates fall within the boundaries of the respective reference peak, the candidate with the smallest difference between corresponding retention time is chosen as a hit. All other peak candidates are subsequently tagged as fail.
  - For the peak reporting ML model, the peaks and their quality scores listed in qslog\_report.tsv are checked against the manually inserted flags of each manual\_peakcheck.tsv (0: Fail, 2: Hit). Subsequently, quantiles of quality scores for each batch and each metabolite are computed and appended to the training data. This means that each peak gets 5 additional values for the ML model to be trained against: The 0th, 25th, 50th, 75th and 100th percentile of the corresponding batch&#39;s quality scores.
- Afterwards, resulting training data is split up into training (80 percent) and testing data (20 percent) for model evaluation after training.
- Missing quality scores for individual peaks or peak candidates are imputed by median values of the respective quality score in the training data set.
- Quality scores and respective quantiles (when training the peak reporting ML model) are scaled, centered and transformed using the Yeo-Johnson transformation.
- The chosen ML model (either Random Forest, Neural Network, Support Vector Machines or Extreme Gradient Boosting) is trained using 3-fold cross validation and ROC-curve as optimization metric.
- The resulting ML model is saved as \*.RData file in &#39;base\_folder&#39;. In case of the peak picking ML model, median values of each quality score from the training data set are appended to the \*.RData file. These median values are needed for imputation during subsequent runs of process\_batch() if calculated quality scores have invalid values (e.g. 0 or NA).
- For evaluation, the ML model is in turn used to predict the testing data set. Model evaluation metrics are visualized/summarized in various graphs which are automatically stored as .pdf or .txt files in the &#39;base\_folder&#39;:
  - Feature density: The plot shows the distribution of each feature (quality scores and their quantiles [in case of the peak reporting model]) for each classification group (blue: no peak, pink: peak). The graph can be utilized as a first intuition about the impact of each quality score on the models ability to correctly separate cases of hit and fail. The feature density plot is created firstly for the training data and secondly for the testing data, the latter is based on evaluated model predictions.
  - False negatives/false positives: The .txt files show all peaks or peak candidates which have been falsely classified as hit (false positive) or fail (false negative) and their respective features (quality scores and their quartiles in case of the peak reporting ML model).
  - Confusion matrix: The confusion matrix shows the summary of correctly and falsely classified peaks as well as several evaualuation metrics (e.g. model accuracy and sensitivity/specificity).
  - Prediction density plot: Probability density distributions for all model predictions.
  - Feature importance: The influence of each quality score on model predictions.
- In case of training the peak picking ML model, process\_batch() is run directly after the model has been trained. process\_batch() will then use the saved model for peak picking predictions to generate the files peakoverview.pdf and manual\_peakcheck\_template.xlsx. These files can subsequently be used to manually prepare the training data for the peak reporting ML model.

<br>

### Arguments:

- model\_type: &quot;pick&quot; (peak picking ML model) or &quot;report&quot; (peak reporting ML model)
- ml\_type: Either &quot;rf&quot; (Random Forest [default]) , &quot;nnet&quot; (Neural Network), &quot;svm&quot; (Support Vector Machines) or &quot;xgbtree&quot; (Extreme Gradient Boosting)
- base\_folder: Working directory that contains named subfolders with training dataset(s).
- datasets: Subfolders of &#39;base\_folder&#39; that must contain all relevant files to run process\_batch() which is called by train\_model().
- model\_file\_name: Tag to automatically name resulting files like \*.RData-files, .txt-files and graphs.

<br>

<br>

## initialize\_prm

initialize\_prm Load and assign global default parameters. Default parameters can be overwritten by the &#39;parameters&#39; argument when calling process\_batch() or using edited update\_prm.tsv files in working directory).

| **Parameter name** | **Default Value** | **Alternative Values** | **Comment** |
| --- | --- | --- | --- |
| ml\_type | mlprod | mltrain\_pick, mltrain\_report | Use process\_batch() in productive mode (default) or in the context of ml\_train |
| expertmode | FALSE | TRUE | Additional debugging output |
| fancyformat | FALSE | TRUE | Add conditional formatting to excel output. Caution, might be incompatible with some versions of MS Excel. |
| verbose | 1 | 0, 2, 3 | Levels of debugging output |
| log\_con | file(&quot;R\_messages.log&quot;, open=&quot;a&quot;) |
 | Connection to log file |
| samplingfrequency | 2 | Positive integer | Frequency in Hz for re-sampling of chromatograms. Should be similar to the rate at which the data has been recorded |
| Corwindow | 5 | Positive integer | Number of data points used for calculating correlation among chromatograms |
| smoothnum | 9 | Positive integer | K for rollmean smoothing of chromatograms |
| minDistTopBorder | 2 | Positive integer | Minimum number of scans between peak top and peak border |
| bgoffset | 42 | Positive integer | Smallest possible signal intensity |
| minNormArea | 10000 | Positive integer | Minimum area of 13C qualifier to perform normalization |
| minwidth | 4 | Positive integer | Minimum width of peak in seconds |
| initial\_candidates | 5 | Positive integer | Number of peak candidates to be evaluated during initial peak picking |
| timewindow | 1.5 | Positive float | Time window in minutes around peak used for retention time alignment |
| tshiftcrude | seq(-60,60,2) | Integer range | Range of allowed retention time shifts |
| microsearch_in | 6 | Integer | Number of scans for fine-adjustment of peak borders during propagation of consensus peak to original chromatograms going from propagated peak boarder towards peak top |
| microsearch_out | 12 | Integer | Number of scans for fine-adjustment of peak borders during propagation of consensus peak to original chromatograms going from propagated peak boarder outwards away from peak top |
| model\_path |
| Filename (incl. path) of file holding peak picking ML model |
| model\_final\_path |
| Filename (incl. path) of file holding peak reporting ML model |

<br>

<br>

# Input data

## Nowfolder (choosen working directory)

- **mzML files:**
  - Measurement files must be present in the working directory mzML format and are read using the mzR package.

<br>

- **sample.info:**
  - the file sample.info is an optional file to attach some additional information to the samples that are going to be processed, such as biological and technical conditions. This information is then used for grouping during the analysis of variance and the generation of the peak report (Peakinfo.xlsx). sample.info is loaded from the working directory and must be present in the form of a comma separated file (other names as sample.info are not allowed). The file must consist of 11 columns and the number of rows must be identical to the number of samples that are going to be analyzed.
  - info must contain the following comma-separated columns:
    - _idmeasurements_: the unique identifier of the \*.mzMl filename (required).
    - _samplename_: the sample name (required).
    - amount: Amount of the material in the sample tube (optional).
    - type1: Used for sample annotation during data analysis. Samples with identical values will be treated as replicates (optional).
    - type2: Used for sample annotation during data analysis. Samples with identical values will be treated as replicates (optional).
    - type3: Used for sample annotation during data analysis. Samples with identical values will be treated as replicates (optional).
    - common\_name: Name of the organism from which the sample was generated (optional).
    - celltypename: Name of celltype of which the sample was generated (optional).
    - cultivationname: Name of the cultivation cindition that was used to generate the sample (optional)
    - measureddate: Date and time of measurement. Must be present in the format YYYY-MM-DD HH:MM:SS (optional)
    - plateposition: Position of the sample tube on the plate (optional)

<br>

- **update\_prm.tsv:**
  - update\_prm.tsv can be used to update the global settings of the analysis. The file is loaded from the working directory and must be present in the form of a tab separated file (other names as update\_prm.tsv are not allowed).
  - There must be exactly two tab separated column names:
    - Variable in prm
    - Value
  - All entries must contain (I) the name of the prm element to be replaced and (II) the respective value to overwrite the default settings (see list of available prm options).
  - Alternatively, prm options can be overwritten with the argument &#39;parameters&#39; when calling the process\_batch function. When process\_batch() is called with given &#39;parameters&#39;, the default settings and values set in update\_prm.tsv are overwritten.

<br>

- **training_solution.xlsx:**
  - The file training_solution.xlsx is used to train the peak picking ML model, which is used applied for initial peak detection. The file is required when train\_modeltrain\_model() is called with the function argument model\_type&#39;model\_type&#39; set to &quot;pick&quot;.
  - This file can be easily generated by expert peak review in Agilent MassHunter Quantitative Analysis.
  - xlsx must be present in the working directory (other names than training_solution.xlsx are not allowed) and is loaded during model training.
  - The first two rows of the file must contain the necessary column names. Columns are hierarchically structured. The first row must contain the following columns:
    - Sample section: Samples are defined in the Data File column.
    - List of metabolite names: The given column names must exactly match those given in the metabolite database set in update\_prm.tsv.
  - The second row must contain the following columns:
    - Data File: Names of the measurement files listed in the working directory (File extensions are automatically omitted).
    - RT: Peak Retention time of the specific metabolite in minutes.
    - Start: Peak start time in minutes.
    - End: Peak end time in minutes.

<br>

- **qslog\_pick.tsv:**
  - qslog\_pick.tsv is used to train the peak picking ML model which is used for initial peak detection.
  - The file is automatically created in case of no present ML models. When the prm setting &#39;ml\_type&#39; is set to &quot;mltrain\_pick&quot; 5 peak candidates are detected for each metabolite and each sample.
  - Quality scores of each peak candidate are calculated and and written to qslog\_pick.tsv which is automatically saved in the working directory.
  - The file&#39;s columns contain information about the metabolite, the sample, the peak candidate number, its retention time and all calculated quality scores.
  - qslog\_inital.tsv is read during train\_model() when the function argument &#39;model\_type&#39; is set to &quot;&#39;pick&quot;&#39;.

<br>

- **manual\_peakcheck.xlsx:**
  - The file manual\_peakcheck.xlsx is used to train the peak reporting ML model, which is used for final peak evaluation. The file is required when train\_model() is called with the function argument &#39;model\_type&#39; set to &quot;report&quot;.
  - The file should be created by the user before the peak reporting ML model is trained. When process\_batch() is run using the peak picking ML model, a template file (manual\_peakcheck\_template.xlsx) is automatically created and saved in the working directory. The file already contains all necessary information except the peaks score. The user can now use the template to fill out the score columns for each metabolite. Hereby the file peakoverview.pdf can be used as a visual reference.
  - manual\_peakcheck.xlsx must be present in the working directory (other names than manual\_peakcheck.xlsx are not allowed) and is loaded during model training.
  - The first two rows ofos the file must contain the necessary column names. Columns are hierarchically structured. The first row must contain the following columns:
    - Names of the measurement files listed in the working directory.
    - List of metabolite names: The given column names must exactly match those given in the metabolite database set in update\_prm.tsv.
  - The second row must contain the following columns:
    - Score: Should the chosenchoosen peak be reported (0: no peak should not be reported | 1: not sureno clear decision can be made | 2: peak should be reportedyes)
    - Start: Peak start time in minutes.
    - End: Peak end time in minutes.

<br>

- **qslog\_report.tsv**
  - tsv is used to train the peak reporting ML model which is used to decide if a given peak should finally be reported.
  - The file is automatically created when the peak picking ML model is alreadyallready trained and used for peak detection by running process\_batch(). When the prm setting &#39;ml\_type&#39; is set to &quot;mltrain\_report&quot; the best peak of each metabolite and each sample is chosen by the peak picking ML model and based on the peak&#39;s quality scores.
  - The chosen peaks are then written to qslog\_report.tsv which is saved in the current working directory. The file&#39;s columns contain information about the metabolite, the sample and the chosenchoosen peak&#39;s quality scores.
  - tsv is read during train\_model() when the function argument &#39;model\_type&#39; is set to &quot;&#39;report&quot;&#39;.

<br>

## Other

- **Metabolite database (excelfile) (excelfile):**
  - The metabolite database is used to configure information about the metabolites to be detected by process\_batch().
  - The table should be present in xlsx format and is automatically read in during process\_batch(). The path of the file must be set using either the parameter argument in the function call of process\_batch() or the assignment of the Excel file variable in update\_prm.tsv.
  - The file must contain the following columns:
    - Name: Name of the metabolite (string format).
    - HMDB: ID of the metabolite in the Human Metabolite Database (string format).
    - KEGG: ID of the metabolite in the Kyoto Encyclopedia of Genes and Genomes (string format).
    - RT: Expected retention time of the metabolite (double format in minutes)
    - Quantifer: Flag if metabolite should be handled as quantifier (integer format, 1: yes | 0: no).
    - UseTrace: Flag if a metabolite&#39;s quantifier/qualifier should be used for the calculation of peak quality scores (integer format, 1: yes | 0: no).
    - Isoptope: Flag if a metabolite&#39;s quantifier/qualifier should be handled as 12C or 13C isotope (string format, &#39;12C&#39; | &#39;13C&#39;).
    - Polarity: Polarity of measurement. The polarity flag is crucial to read out the respective MRM of the \*.mzML file (string format, &#39;Positive&#39; | &#39;Negative&#39;).
    - Q1mz: Expected mass to charge ratio for the first quadrupole. This column is crucial to read out the respective MRM of the \*.mzML file (double format).
    - Q3mz: Expected mass to charge ratio for the second quadrupole. This column is crucial to read out the respective MRM of the \*.mzML file (Ddouble format)
    - Abundance: Expected relative ratio of the metabolite&#39;s quantifier/qualifier compared to other quantifiers/qualifiers of the respective metabolite. (Ddouble format).

<br>

- **Peak picking ML model (model\_path):**
  - The peak picking ML model must be trained by train\_modeltrain\_model() initially and is then stored as \*.RData file in the folder given by the function&#39;s argument base\_folderbase\_folder and the filename set by the function&#39;s argument &#39;model\_file\_name&#39;.
  - Once trained, the peak picking ML model can be used to detect the best peak out of the 5 given candidates for each sample and each metbolite during process\_batch(). The peak picking ML model is not only used to predict peak qualities but also to prepocess raw quality scores in the forefield (missing data imputation and normalization). Therefore, process\_batch() has to be called with the argument &#39;ml\_type&#39; set to &quot;mltrain\_report&quot;.
  - The path of the peak picking ML model file must be set using either the &#39;parameter&#39; argument in the function call of process\_batch() or the assigment of the &#39;excelfile&#39; variable in update\_prm.tsv.

<br>

- **Peak reporting ML model (model\_final\_path):**
  - The peak reporting model must be trained by train\_modeltrain\_model() initially and is then stored as \*.RData file in the folder given by the function&#39;s argument base\_folder and the filename set by the function&#39;s argument &#39;model\_file\_name&#39;.
  - Once trained, the peak reporting ML model can be used to evaluate if the peak which was detected by the peak picking ML model should be reported for each sample and each metabolite during process\_batch(). The peak reporting ML model is not only used to predict peak qualities but also to prepocess raw quality scores in the forefield (missing data imputation and normalization). Therefore, process\_batch() has to be called with the argument &#39;ml\_type&#39; set to &quot;mlprod&quot;.
  - The path of the peak reporting ML model file must be set using either the &#39;parameter&#39; argument in the function call of process\_batch() or the assignment of the &#39;excelfile&#39; variable in update\_prm.tsv

<br>

# Output data

## Nowfolder (chosenchoosen working directory)

- **peakinfo.xlsx:**
  - xlsx holds the results of the analysis. The files columns consist of (I) metainformation about the batch when provided by sample.info and (II) the metabolites listed in. All processed samples are listed row-wise showing the calculated abundances of each metabolite.
  - The spreadsheet is divided in six/eight tables:
    - NormArea
    - NormHeight
    - RawArea
    - Rawheight
    - 13Carea
    - LuckyDip (all quantifier peak areas, not filtered by score of peak reporting ML model)
    - Lucky13C (all 13C qualifier peak areas, not filtered by score of peak reporting ML model)
    - Info

<br>

- **R.time:**
  - time is automatically written to the working directory and stores the time of analysis of process\_batch().

<br>

- **peakoverview.pdf:**
  - All chromatograms of measured metabolites (rows) in each sample (columns) are graphically shown in peakoverview.pdf.
  - Quantifier chromatograms are colored in black, whereas 12C qualifiers are shown in blue and 13C qualifiers are colored in green. The left x-axis refers to quantifier intensities (black), the right one refers to qualifier intensities (green).
  - The legend in each plot shows the expected polarities and mass to charge ratios of each quantifier and qualifier (if present).
  - When running in expertmode, the labels of each graph show the names of the metabolite and sample as well as the detailed quality scores of each peak. In case expertmode is turned off, only the aggregated quality score of each peak is shown in the labels.
  - The expected retention time of each peak is visualized as red mark on the x-axis, the shifted retention time that is expected is shown as blue mark.
  - The calculated peak retention time for each metabolite and each sample is indicated as a vertical dotted line. The area of the peak is shaded gray.

<br>

- **R\_messages.log:**
  - R\_messages.R serves as the main file for logging the analysis of process\_batch() and all its subfunctions, depending on the chosen level of the global parameter verbose.

<br>

- **shiftplots.pdf:** **(format axes | explain consensus\_peaks alignment | saving PDF necessary?):**
  - The PDF-file shows the shift of all metabolite peak&#39;s retention times for each sample.
  - The x-axis shows the peaks&#39; calculated retention times, on the y-axis the respective shifts.
  - The calculated model used for peak aligment is visualized as blue line lines.
  - format axes
  - saving PDF necessary (verbose level 3)
  - Should not be documented

<br>

- **troubleshoot.RData:**
  - ?

<br>

- **manual\_peakcheck\_template.xlsx:**
  - When process\_batch() is run using the peak picking ML model, a template file (manual\_peakcheck\_template.xlsx) is automatically created and saved in the working directory. The file already contains all necessary information except the peaks score. The user can now use the template to fill out the score columns for each metabolite. Hereby the file peakoverview.pdf can be used as a visual reference. After the training data has been inserted the file can then be saved as manual\_peakcheck.xlsx and will be processed to train the peak reporting ML model.

<br>

- **qslog\_pick.tsv:**
  - qslog\_pick.tsv is used to train the peak picking ML model which is used for initial peak detection.
  - The file automatically created in case of no present ML models. When the prm setting ml\_type is set to mltrain\_pick 5 peak candidates are detected for each metabolite and each sample.
  - Quality scores of each peak candidate are calculated and and written to qslog\_pick.tsv which is automatically saved in the working directory.
  - The file&#39;s columns contain information about the metabolite, the sample, the peak candidate number, its retention time and all calculated quality scores.
  - qslog\_inital.tsv is read during train\_modeltrain\_model() when the function argument model\_type&#39;model\_type&#39; is set to &quot;&#39;pick&quot;&#39; .

<br>

- **qslog\_report.tsv:**
  - tsv is used to train the peak reporting ML model which is used to decide if a given peak should finally be reported.
  - The file is automatically created when the peak candidate model is alreadyallready trained and used for peak detection by running process\_batch(). When the prm setting &#39;ml\_type&#39; is set to &quot;mltrain\_report&quot; the best peak of each metabolite and each sample is chosen by the peak picking ML model and based on the peak&#39;s quality scores.
  - The chosenchoosen peaks are then written to qslog\_report.tsv which is saved in the current working directory. The file&#39;s columns contain information about the metabolite, the sample and the chosenchoosen peak&#39;s quality scores.
  - tsv is read during train\_modeltrain\_model() when the function argument model\_type&#39;model\_type&#39; is set to &quot;&#39;report&quot;&#39;.

## Other

- **Peak picking ML model (model\_path):**
  - The peak candidate model must be trained by train\_modeltrain\_model() initially and is then stored as \*.RData file in the folder given by the function&#39;s argument &#39;base\_folder&#39; and the filename set by the function&#39;s argument &#39;model\_file\_name&#39;.
  - Once trained, the peak picking ML model can be used to detect the best peak out of the 5 given candidates for each sample and each metabolite during process\_batch(). The peak picking ML model is not only used to predict peak qualities but also to preprocess raw quality scores in the forefield (missing data imputation and normalization). Therefore, process\_batch() has to be called with the argument &#39;ml\_type&#39; set to &quot;mltrain\_report&quot;.
  - The path of the peak picking ML model file must be set using either the &#39;parameter&#39; argument in the function call of process\_batch() or the assignment of the &#39;excelfile&#39; variable in update\_prm.tsv.

<br>

- **Peak reporting ML model (model\_final\_path):**
  - The peak reporting model must be trained by train\_modeltrain\_model() initially and is then stored as \*.RData file in the folder given by the function&#39;s argument &#39;base\_folder&#39; and the filename set by the function&#39;s argument &#39;model\_file\_name&#39;.
  - Once trained, the peak reporting ML model can be used to evaluate if the peak which was detected by the peak picking ML model should be reported for each sample and each metabolite during process\_batch(). The peak reporting ML model is not only used to predict peak qualities but also to prepocess raw quality scores in the forefield (missing data imputation and normalization). Therefore, process\_batch() has to be called with the argument &#39;ml\_type&#39; set to &quot;mlprod&quot;.
  - The path of the peak reporting ML model file must be set using either the &#39;parameter&#39; argument in the function call of process\_batch() or the assigment of the &#39;excelfile&#39; variable in update\_prm.tsv.

