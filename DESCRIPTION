Package: automRm
Title: Fully automatic processing of LC-QQQ data from .mzML to .xlsx
Version: 1.1.5
Authors@R: 
    person(given = "Joerg",
           family = "Buescher",
           role = c("aut", "cre"),
           email = "buescher@ie-freiburg.mpg.de",
           comment = c(ORCID = "0000-0002-6547-0076"))
    person(given = "Daniel",
           family = "Eilertz",
           role = c("aut", "cre"),
           email = "eilertz@ie-freiburg.mpg.de")
Description: automRm processes LC-QQQ raw data in the open .mzML format to obtain a user-friendly output of signal intensities for every analyte and every sample in .xlsx format. In addition to the .mzML files a matching list of target metabolites must be available. automRm then uses 2 random forest models to 1st. decide which chromatographic peak is the most likely to represent an analyte and 2nd to decide if the data is of sufficient quality to be given to a person with little metabolomics experience. Both random forest models can easily be trained to meet one's LC-MS methods and quality standards.
License: MIT + file LICENSE
Encoding: UTF-8
LazyData: true
Roxygen: list(markdown = TRUE)
RoxygenNote: 7.2.3
biocViews:
Imports:
	caret (>= 6.0-86),
	e1071 (>= 1.7-3),
	kernlab (>= 0.9-27),
	mgcv(>= 1.8-28),
	mzR (>= 2.20.0),
	neuralnet (>= 1.44.2),
	nnls (>= 1.4),
	openxlsx (>= 4.2.3),
	randomForest (>= 4.6-14),
	robustbase (>= 0.93-6),
	xgboost (>= 0.82.1),
	zoo (>= 1.8-7),
	shiny (>= 1.7.1),
	keys (>= 0.1.1),
	shinyFiles (>= 0.8.0)
